@file:OptIn(ExperimentalCoroutinesApi::class)

package eu.jakubmisko.driker.screen.history

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import eu.jakubmisko.driker.domain.DrinkType
import eu.jakubmisko.driker.domain.HistoryFilterData
import eu.jakubmisko.driker.domain.Record
import eu.jakubmisko.driker.repository.RecordRepository
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.time.OffsetDateTime


class HistoryViewModel(private val recordRepository: RecordRepository) : ViewModel() {
    private val searchData = MutableStateFlow(DataType.ALL)
    private val allRecords: Flow<PagingData<Record>> = recordRepository.getDrinkRecordsPaged()
        .cachedIn(viewModelScope)
    private var fullTextFilteredRecords: Flow<PagingData<Record>> = emptyFlow()
    private var dateTimeFilteredRecords: Flow<PagingData<Record>> = emptyFlow()
    private val deleteDisposable = CompositeDisposable()
    private val _filter = MutableStateFlow(HistoryFilterData())
    val filter: StateFlow<HistoryFilterData> = _filter

    fun updateFilter(function: (HistoryFilterData) -> HistoryFilterData) {
        _filter.update(function)
    }

    fun getDrinkRecords(): Flow<PagingData<Record>> = searchData.flatMapLatest {
        when (it) {
            DataType.FILTER -> dateTimeFilteredRecords
            DataType.FULLTEXT -> fullTextFilteredRecords
            DataType.ALL -> allRecords
        }
    }

    fun delete(record: Record) {
        viewModelScope.launch {
            recordRepository.deleteRecord(record)
        }
    }

    fun search(query: String?) {
        query?.takeIf { it.isNotBlank() }
            ?.let {
                fullTextFilteredRecords = recordRepository.searchRecords(it)
                searchData.value = DataType.FULLTEXT
            } ?: run {
            searchData.value = DataType.ALL
        }
    }

    fun applyFilter() {
        when {
            (filter.value.from != null || filter.value.to != null) && filter.value.drinkTypes != null -> {
                applyTimeAndTypeFilter(
                    filter.value.from ?: OffsetDateTime.MIN,
                    filter.value.to ?: OffsetDateTime.MAX,
                    filter.value.drinkTypes!!
                )
                searchData.value = DataType.FILTER
            }

            filter.value.from != null || filter.value.to != null -> {
                applyTimeFilter(
                    filter.value.from ?: OffsetDateTime.MIN,
                    filter.value.to ?: OffsetDateTime.MAX
                )
                searchData.value = DataType.FILTER
            }

            filter.value.drinkTypes != null -> {
                applyTypeFilter(filter.value.drinkTypes!!)
                searchData.value = DataType.FILTER
            }

            else -> {
                searchData.value = DataType.ALL
            }
        }
    }


    private fun applyTimeFilter(from: OffsetDateTime, to: OffsetDateTime) {
        dateTimeFilteredRecords = recordRepository.searchRecordsInRange(from, to)
    }

    private fun applyTimeAndTypeFilter(
        from: OffsetDateTime,
        to: OffsetDateTime,
        drinkType: Set<DrinkType>
    ) {
        dateTimeFilteredRecords = recordRepository.searchRecordsInRange(from, to, drinkType)
    }

    private fun applyTypeFilter(drinkType: Set<DrinkType>) {
        dateTimeFilteredRecords = recordRepository.searchRecords(drinkType)
    }

    override fun onCleared() {
        deleteDisposable.clear()
    }

    enum class DataType { FILTER, FULLTEXT, ALL }

    enum class DeleteEvent { SUCCESS, FAILURE }
}