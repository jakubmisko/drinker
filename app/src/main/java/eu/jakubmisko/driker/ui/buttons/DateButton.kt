@file:OptIn(ExperimentalMaterial3Api::class)

package eu.jakubmisko.driker.ui.buttons

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CalendarLocale
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDefaults
import androidx.compose.material3.DatePickerFormatter
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import eu.jakubmisko.driker.R
import eu.jakubmisko.driker.ui.DayIcon
import eu.jakubmisko.driker.ui.popup.DateTimePopup

@Preview
@Composable
fun DateButtonPreview() {
    DateButton(797897879789L, { "12.2.2025" }, { })
}

@Composable
fun DateButton(
    initialMillis: Long,
    formatDate: (Long) -> String,
    onDateChanged: (millis: Long?) -> Unit,
    modifier: Modifier = Modifier
) {
    var showDatePicker by remember { mutableStateOf(false) }
    val datePickerState = rememberDatePickerState(initialSelectedDateMillis = initialMillis)
    Button(
        modifier = modifier,
        onClick = { showDatePicker = !showDatePicker },
        colors = ButtonDefaults.textButtonColors(),
        contentPadding = PaddingValues(horizontal = 4.dp)
    ) {
        Row {
            DayIcon()
            Text(
                formatDate(initialMillis),
                modifier = Modifier.padding(start = 8.dp)
            )
        }
    }
    if (showDatePicker) {
        DateTimePopup(
            onDismiss = { showDatePicker = false },
            onApply = {
                onDateChanged(datePickerState.selectedDateMillis)
            },
            title = stringResource(R.string.select_date)
        ) {
            DatePicker(
                state = datePickerState,
                showModeToggle = true,
                colors = DatePickerDefaults.colors(containerColor = MaterialTheme.colorScheme.surfaceContainer),
                dateFormatter = object : DatePickerFormatter {
                    override fun formatDate(
                        dateMillis: Long?,
                        locale: CalendarLocale,
                        forContentDescription: Boolean
                    ): String? = dateMillis?.let(formatDate)

                    override fun formatMonthYear(
                        monthMillis: Long?,
                        locale: CalendarLocale
                    ): String? = null
                })
        }
    }
}