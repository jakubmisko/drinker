package eu.jakubmisko.driker.repository

import eu.jakubmisko.driker.domain.Drink
import eu.jakubmisko.driker.domain.DrinkType
import eu.jakubmisko.driker.persistence.Db
import eu.jakubmisko.driker.persistence.model.DrinkEntity
import eu.jakubmisko.driker.persistence.model.DrinkTypeEntity
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Test

class DrinkRepositoryTests {
    private val db: Db = mockk()
    private val repo = DrinkRepositoryImpl(db)
    private val drink = Drink(0.5, "l", "Pivo", DrinkType.BEER, 1)

    @Test
    fun `add drink`() {
        every { db.getDrinkDao().insertDrink(any()) } returns Single.just(1)

        repo.addDrink(drink)
                .test()
                .apply { awaitTerminalEvent() }
                .assertNoErrors()
                .assertComplete()
                .assertValue(1)
        verify(exactly = 1) { db.getDrinkDao().insertDrink(any()) }
    }

    @Test
    fun `update drink`() {
        every { db.getDrinkDao().updateDrink(any()) } returns Completable.complete()

        repo.updateDrink(drink)
                .test()
                .apply { awaitTerminalEvent() }
                .assertNoErrors()
                .assertComplete()
        verify(exactly = 1) { db.getDrinkDao().updateDrink(any()) }
    }

    @Test
    fun `delete drink`() {
        every { db.getDrinkDao().deleteDrink(any()) } returns Completable.complete()

        repo.deleteDrink(drink)
                .test()
                .apply { awaitTerminalEvent() }
                .assertNoErrors()
                .assertComplete()
        verify(exactly = 1) { db.getDrinkDao().deleteDrink(any()) }
    }

    @Test
    fun `get drink by id`() {
        every { db.getDrinkDao().getDrink(any()) } returns Observable.just(DrinkEntity(0.5, "l", "Pivo", DrinkTypeEntity.BEER, 1))

        repo.getDrink(1)
                .test()
                .apply { awaitTerminalEvent() }
                .assertNoErrors()
                .assertComplete()
                .assertValue(drink)
        verify(exactly = 1) { db.getDrinkDao().getDrink(any()) }
    }

    @Test
    fun `gel all drinks`() {
        every { db.getDrinkDao().getAllDrinks() } returns Observable.just(listOf(DrinkEntity(0.5, "l", "Pivo", DrinkTypeEntity.BEER, 1)))

        repo.getAllDrinks()
                .test()
                .apply { awaitTerminalEvent() }
                .assertNoErrors()
                .assertComplete()
                .assertValue(listOf(drink))

        verify(exactly = 1) { db.getDrinkDao().getAllDrinks() }
    }

    @Test
    fun `search drinks`() {
        every { db.getDrinkDao().searchDrinks(any()) } returns Observable.just(listOf(DrinkEntity(0.5, "l", "Pivo", DrinkTypeEntity.BEER, 1)))

        repo.searchDrinks("query")
                .test()
                .apply { awaitTerminalEvent() }
                .assertNoErrors()
                .assertComplete()
                .assertValue(listOf(drink))
        verify(exactly = 1) { db.getDrinkDao().searchDrinks(any()) }
    }
}