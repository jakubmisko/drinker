@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class)

package eu.jakubmisko.driker.screen.add

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.MenuAnchorType
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Slider
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.compose.LocalLifecycleOwner
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import eu.jakubmisko.driker.R
import eu.jakubmisko.driker.domain.Drink
import eu.jakubmisko.driker.domain.isFailure
import eu.jakubmisko.driker.domain.isSuccess
import eu.jakubmisko.driker.tag
import eu.jakubmisko.driker.ui.buttons.DateButton
import eu.jakubmisko.driker.ui.buttons.TimeButton
import eu.jakubmisko.driker.ui.theme.AppTheme
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.android.ext.android.get
import java.time.Instant
import java.time.LocalDate
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.util.concurrent.TimeUnit


class AddEditRecordFragment : Fragment() {

    private val viewModel: AddEditRecordViewModel by viewModels(factoryProducer = { get<AddRecordViewModelFactory>() })
    private val args: AddEditRecordFragmentArgs by lazy {
        AddEditRecordFragmentArgs.fromBundle(
            requireArguments()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(
        requireContext()
    ).apply {
        setContent {
            val state by viewModel.state.collectAsStateWithLifecycle()
            val availableDrinks by viewModel.availableDrinks.collectAsStateWithLifecycle()
            val snackbarHostState = remember { SnackbarHostState() }
            val lifecycleOwner = LocalLifecycleOwner.current

            Log.d(tag(), "Add edit record state=$state")

            DisposableEffect(lifecycleOwner, viewModel.events) {
                val job = viewModel.events
                    .flowWithLifecycle(lifecycleOwner.lifecycle)
                    .onEach {
                        Log.d(tag(), "Add edit record event=$it")

                        it.isSuccess()?.let {
                            findNavController().navigateUp()
                        } ?: it.isFailure()?.let {
                            snackbarHostState.showSnackbar(
                                message = getString(R.string.failed_to_save),
                                actionLabel = getString(android.R.string.ok)
                            )
                        }
                    }
                    .launchIn(lifecycleOwner.lifecycleScope)

                onDispose {
                    job.cancel()
                }
            }
            AppTheme {
                DrinkRecordPage(
                    state = state,
                    availableDrinks = availableDrinks,
                    snackbarHostState = snackbarHostState,
                    formatDate = { millis ->
                        viewModel.formatDate(
                            LocalDate.ofEpochDay(
                                TimeUnit.MILLISECONDS.toDays(millis)
                            )
                        )
                    },
                    formatTime = { hour, minute ->
                        viewModel.formatTime(
                            LocalTime.of(
                                hour,
                                minute
                            )
                        )
                    },
                    updateState = { newState -> viewModel.updateState { newState } },
                    add = { viewModel.persist() },
                    navigateBack = { findNavController().navigateUp() }
                )
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        args.editDrink?.run {
            viewModel.updateState {
                RecordState(
                    drink = drink,
                    dateTime = time,
                    quantity = quantity,
                    comment = comment,
                    recordId = id
                )
            }
        }
    }


    @Preview
    @Composable
    fun DrinkPagePreview() {
        DrinkRecordPage(
            state = RecordState(),
            availableDrinks = emptyList(),
            formatDate = { "1.1.2025" },
            formatTime = { _, _ -> "1:1" },
            updateState = { RecordState() },
            add = {},
            navigateBack = {}
        )
    }

    @Composable
    fun DrinkRecordPage(
        state: RecordState,
        availableDrinks: List<Drink>,
        snackbarHostState: SnackbarHostState = remember { SnackbarHostState() },
        formatDate: (Long) -> String,
        formatTime: (hour: Int, minute: Int) -> String,
        updateState: (RecordState) -> Unit,
        add: () -> Unit,
        navigateBack: () -> Unit,
    ) {
        Scaffold(
//            topBar = {
//                TopAppBar(title = {
//                    Text(stringResource(R.string.what_did_you_drank))
//                },
//                    navigationIcon = {
//                        IconButton(onClick = navigateBack) {
//                            Icon(Icons.AutoMirrored.Sharp.ArrowBack, contentDescription = "Back")
//                        }
//                    })
//            },
            snackbarHost = {
                SnackbarHost(hostState = snackbarHostState)
            }) { padding ->
            Column(
                Modifier
                    .padding(padding)
                    .padding(horizontal = 16.dp)
                    .fillMaxSize()
            ) {
                Box(Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
                    Image(
                        modifier = Modifier.fillMaxSize(0.3f),
                        painter = painterResource(
                            state.drink?.type?.image ?: R.drawable.ic_question_mark
                        ),
                        contentDescription = "Drink image"
                    )
                }
                DrinkSelectTextField(
                    modifier = Modifier.fillMaxWidth(),
                    selectedDrink = state.drink?.name.orEmpty(),
                    availableDrinks = availableDrinks,
                    onSelected = { drink ->
                        updateState(state.copy(drink = drink))
                    })
                DateButton(
                    initialMillis = state.dateTime.toEpochSecond() * 1000,
                    formatDate = formatDate,
                    onDateChanged = { millis -> updateDate(millis, state)?.let(updateState) })
                TimeButton(
                    initialHour = state.dateTime.hour,
                    initialMinute = state.dateTime.minute,
                    formatTime = formatTime,
                    onTimeChanged = { hour: Int, minute: Int ->
                        updateState(
                            state.copy(
                                dateTime = state.dateTime.withHour(hour).withMinute(minute)
                            )
                        )
                    })
                DrinkQuantity { quantity ->
                    updateState(
                        state.copy(quantity = quantity)
                    )
                }
                CommentTextField(
                    state = state.comment,
                    onUpdate = { newComment ->
                        updateState(state.copy(comment = newComment))
                    },
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(Modifier.height(16.dp))
                Button(modifier = Modifier.fillMaxWidth(), onClick = add) {
                    Text(stringResource(R.string.add))
                }
            }
        }
    }

    private fun updateDate(millis: Long?, state: RecordState): RecordState? = millis?.let {
        OffsetDateTime.ofInstant(
            Instant.ofEpochMilli(it),
            ZoneOffset.systemDefault()
        ).withHour(state.dateTime.hour)
            .withMinute(state.dateTime.minute)
    }?.let { offsetDateTime ->
        state.copy(
            dateTime = offsetDateTime
        )
    }

    @Composable
    fun DrinkSelectTextField(
        selectedDrink: String,
        availableDrinks: List<Drink>,
        onSelected: (Drink) -> Unit,
        modifier: Modifier = Modifier
    ) {
        var isExpanded by remember { mutableStateOf(false) }
        var searchQuery by remember { mutableStateOf(selectedDrink) }

        Log.d(tag(), "DrinkSelectTextField: isExpanded=$isExpanded query=$searchQuery")
        ExposedDropdownMenuBox(
            modifier = modifier,
            expanded = isExpanded,
            onExpandedChange = { isExpanded = !isExpanded }
        ) {
            OutlinedTextField(
                modifier = modifier.menuAnchor(MenuAnchorType.PrimaryEditable),
                value = searchQuery,
                onValueChange = { searchQuery = it },
                label = { Text(stringResource(R.string.choose_drink)) },
                trailingIcon = {
                    ExposedDropdownMenuDefaults.TrailingIcon(expanded = isExpanded)
                },
            )
            val filterOpts = if (searchQuery.isNotBlank() || isExpanded) availableDrinks.filter {
                it.name.contains(searchQuery, ignoreCase = true)
            } else
                emptyList()
            val showResults = filterOpts.isNotEmpty()
                    && (filterOpts.size > 1 || filterOpts.first().name != selectedDrink)
            ExposedDropdownMenu(
                expanded = isExpanded && showResults,
                onDismissRequest = { isExpanded = false },
            ) {
                filterOpts.forEach { option ->
                    DropdownMenuItem(
                        text = {
                            Text(text = option.name)
                        },
                        onClick = {
                            onSelected(option)
                            searchQuery = option.name
                            isExpanded = false
                        }
                    )
                }
            }
        }
    }

    @Composable
    fun DrinkQuantity(onUpdate: (Int) -> Unit) {
        var sliderPosition by remember { mutableFloatStateOf(1f) }
        Column(Modifier.fillMaxWidth()) {
            Text(text = stringResource(R.string.quantity))
            Slider(
                value = sliderPosition,
                onValueChange = { sliderPosition = it },
                onValueChangeFinished = { onUpdate(sliderPosition.toInt()) },
                steps = 10,
                valueRange = 1f..10f
            )
            Text(text = sliderPosition.toInt().toString())
        }
    }

    @Composable
    fun CommentTextField(state: String, onUpdate: (String) -> Unit, modifier: Modifier = Modifier) {
        OutlinedTextField(
            modifier = modifier,
            value = state,
            onValueChange = onUpdate,
            label = { Text(stringResource(R.string.comment)) })
    }
}