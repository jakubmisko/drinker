package eu.jakubmisko.driker

import android.app.Application
import androidx.room.Room
import eu.jakubmisko.driker.persistence.Db
import eu.jakubmisko.driker.persistence.migrations.Initialization
import eu.jakubmisko.driker.repository.DrinkRepository
import eu.jakubmisko.driker.repository.DrinkRepositoryImpl
import eu.jakubmisko.driker.repository.RecordRepository
import eu.jakubmisko.driker.repository.RecordRepositoryImpl
import eu.jakubmisko.driker.screen.add.AddRecordViewModelFactory
import eu.jakubmisko.driker.screen.history.HistoryViewModelFactory
import org.koin.android.ext.android.get
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.qualifier.named
import org.koin.dsl.module
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.Locale


class App : Application() {

    private val viewModels = module {
        single {
            createRoom()
        }
        single<DrinkRepository> { DrinkRepositoryImpl(get()) }
        single<RecordRepository> { RecordRepositoryImpl(get()) }
        single { HistoryViewModelFactory(get()) }
        single { AddRecordViewModelFactory(get(), get(), get(named("date")), get(named("time"))) }
    }

    private val localization = module {
        single { getLocale() }
        factory { DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT) }
        factory(qualifier = named("time")) { DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT) }
        factory(qualifier = named("date")) { DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT) }
    }

    private fun createRoom(): Db {
        return Room.databaseBuilder(get(), Db::class.java, "database")
            .addCallback(Initialization)
            .fallbackToDestructiveMigration()
            .build()
    }

    private fun getLocale(): Locale {
        return resources.configuration.locales.get(0)
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(listOf(viewModels, localization))
        }
    }
}