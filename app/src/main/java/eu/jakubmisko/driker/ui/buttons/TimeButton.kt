@file:OptIn(ExperimentalMaterial3Api::class)

package eu.jakubmisko.driker.ui.buttons

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TimePicker
import androidx.compose.material3.TimePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import eu.jakubmisko.driker.R
import eu.jakubmisko.driker.ui.TimeIcon
import eu.jakubmisko.driker.ui.popup.DateTimePopup

@Composable
@Preview
fun TimeButtonPreview() {
    TimeButton(1, 1, { _, _ -> "1:1" }, { _, _ -> })
}

@Composable
fun TimeButton(
    initialHour: Int,
    initialMinute: Int,
    formatTime: (hour: Int, minute: Int) -> String,
    onTimeChanged: (hour: Int, minute: Int) -> Unit,
    modifier: Modifier = Modifier,
    is24Hour: Boolean = true,
) {
    var showTimePicker by remember { mutableStateOf(false) }
    val timePickerState = TimePickerState(
        initialMinute = initialMinute,
        initialHour = initialHour,
        is24Hour = is24Hour
    )
    Button(
        modifier = modifier,
        onClick = { showTimePicker = !showTimePicker },
        colors = ButtonDefaults.textButtonColors(),
        contentPadding = PaddingValues(horizontal = 4.dp)
    ) {
        Row {
            TimeIcon()
            Text(
                formatTime(
                    timePickerState.hour,
                    timePickerState.minute
                ),
                modifier = Modifier.padding(start = 8.dp)
            )
        }
    }

    if (showTimePicker) {
        DateTimePopup(
            onDismiss = { showTimePicker = false },
            onApply = {
                onTimeChanged(timePickerState.hour, timePickerState.minute)
            },
            title = stringResource(R.string.select_time)
        ) {
            Box(Modifier.fillMaxWidth(), contentAlignment = Center) {
                TimePicker(
                    state = timePickerState,
                )
            }
        }
    }
}