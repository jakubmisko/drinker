package eu.jakubmisko.driker.screen.history.row

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import eu.jakubmisko.driker.R
import eu.jakubmisko.driker.domain.Drink
import eu.jakubmisko.driker.domain.DrinkType
import eu.jakubmisko.driker.domain.Record
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

@Preview(showBackground = true)
@Composable
fun HistoryRowPreview() {
    HistoryRow(
        Record(
            Drink(0.2, "l", "Red wine", DrinkType.WINE),
            OffsetDateTime.now(),
            1,
            "Comment"
        ),
        DateTimeFormatter.ISO_DATE,
        {}
    )
}

@Composable
fun HistoryRow(
    record: Record,
    dateTimeFormatter: DateTimeFormatter,
    onClick: (Record) -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .clickable(onClick = { onClick(record) })
            .padding(16.dp),
        verticalAlignment = Alignment.Top,
    ) {
        Icon(
            painter = painterResource(record.drink.type.image),
            contentDescription = "Drink icon",
            modifier = Modifier.size(56.dp)
        )
        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Column(Modifier.padding(start = 8.dp)) {
                Text(text = record.drink.name, fontWeight = FontWeight.Bold)
                Text(text = record.drink.run {
                    stringResource(
                        R.string.value_space_value,
                        volume,
                        volumeUnit
                    )
                })
            }
            Column(horizontalAlignment = Alignment.End) {
                Text(text = dateTimeFormatter.format(record.time))
                Text(text = record.quantity.toString())
            }
        }
    }
}