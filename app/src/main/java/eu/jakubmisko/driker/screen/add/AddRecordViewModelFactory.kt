package eu.jakubmisko.driker.screen.add

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import eu.jakubmisko.driker.repository.DrinkRepository
import eu.jakubmisko.driker.repository.RecordRepository
import java.time.format.DateTimeFormatter

class AddRecordViewModelFactory(
    private val drinkRepository: DrinkRepository,
    private val recordRepository: RecordRepository,
    private val dateFormatter: DateTimeFormatter,
    private val timeFormatter: DateTimeFormatter,
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        AddEditRecordViewModel(drinkRepository, recordRepository, dateFormatter, timeFormatter) as T

}