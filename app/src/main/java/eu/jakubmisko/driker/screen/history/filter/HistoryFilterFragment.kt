@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)

package eu.jakubmisko.driker.screen.history.filter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.sharp.ArrowBack
import androidx.compose.material.icons.sharp.Check
import androidx.compose.material.icons.sharp.Done
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FilterChipDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.fragment.findNavController
import eu.jakubmisko.driker.R
import eu.jakubmisko.driker.domain.DrinkType
import eu.jakubmisko.driker.domain.HistoryFilterData
import eu.jakubmisko.driker.screen.history.HistoryViewModel
import eu.jakubmisko.driker.ui.buttons.DateButton
import eu.jakubmisko.driker.ui.buttons.TimeButton
import eu.jakubmisko.driker.ui.theme.AppTheme
import org.koin.android.ext.android.inject
import org.koin.core.qualifier.named
import java.time.Instant
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class HistoryFilterFragment : DialogFragment() {
    private val dateFormatter: DateTimeFormatter by inject(named("date"))
    private val timeFormatter: DateTimeFormatter by inject(named("time"))
    private val formatDate: (Long) -> String =
        { dateFormatter.format(OffsetDateTime.ofInstant(Instant.ofEpochMilli(it), ZoneOffset.UTC)) }
    private val formatTime: (hour: Int, minute: Int) -> String =
        { hour, minute -> timeFormatter.format(LocalTime.of(hour, minute)) }
    private val viewModel: HistoryViewModel by viewModels(ownerProducer = { requireActivity() })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(
            requireContext()
        ).apply {
            setContent {
                val filterState by viewModel.filter.collectAsStateWithLifecycle()

                AppTheme {
                    HistoryFilterPage(
                        formatTime,
                        formatDate,
                        from = filterState.from ?: OffsetDateTime.now(),
                        to = filterState.to ?: OffsetDateTime.now(),
                        chipSelectionState = filterState.drinkTypes.toMap(),
                        onDrinkTypeClick = ::onChipClick,
                        onApplyFilter = {
                            viewModel.applyFilter()
                            findNavController().navigateUp()
                        },
                        onClear = {
                            //todo clear dates state
                            viewModel.updateFilter { HistoryFilterData() }
                        },
                        onBack = { findNavController().navigateUp() }

                    )
                }
            }
        }
    }

    private fun Set<DrinkType>?.toMap(): Map<DrinkType, Boolean> = buildMap {
        DrinkType.entries.forEach {
            put(it, false)
        }
        this@toMap?.forEach {
            put(it, true)
        }
    }

    @Composable
    @Preview
    fun Preview() {
        HistoryFilterPage(
            { _, _ -> "1:1" },
            { _ -> "1.1.2025" },
            OffsetDateTime.now(),
            OffsetDateTime.now(),
            mapOf(DrinkType.BEER to true),
            {},
            {},
            {},
            {}
        )
    }


    @Composable
    fun HistoryFilterPage(
        formatTime: (hour: Int, minute: Int) -> String,
        formatDate: (Long) -> String,
        from: OffsetDateTime,
        to: OffsetDateTime,
        chipSelectionState: Map<DrinkType, Boolean>,
        onDrinkTypeClick: (DrinkType) -> Unit,
        onClear: () -> Unit,
        onApplyFilter: () -> Unit,
        onBack: () -> Unit,
    ) {
        Scaffold(
            topBar = {
                TopAppBar(
                    title = {
                        Text(stringResource(R.string.filter))
                    },
                    navigationIcon = {
                        IconButton(onClick = onBack) {
                            Icon(
                                imageVector = Icons.AutoMirrored.Sharp.ArrowBack,
                                contentDescription = "Back"
                            )
                        }
                    },
                    actions = {
                        IconButton(onClick = onApplyFilter) {
                            Icon(
                                imageVector = Icons.Sharp.Check,
                                contentDescription = "Apply"
                            )
                        }
                    }
                )
            }
        ) { padding ->
            Column(
                modifier = Modifier
                    .padding(padding)
                    .padding(horizontal = 16.dp),
            ) {
                Text(
                    stringResource(R.string.from),
                    style = LocalTextStyle.current.copy(fontWeight = FontWeight.Bold)
                )
                DateButton(
                    from.toEpochSecond() * 1000,
                    formatDate,
                    onDateChanged = {
                        storeFromDateTime(
                            OffsetDateTime.ofInstant(
                                Instant.ofEpochMilli(it ?: 0),
                                ZoneOffset.UTC
                            )
                        )
                    }
                )
                TimeButton(
                    from.hour,
                    from.minute,
                    formatTime,
                    onTimeChanged = { hour, minute ->
                        storeFromTime(hour, minute)
                    }
                )

                Text(
                    stringResource(R.string.to),
                    style = LocalTextStyle.current.copy(fontWeight = FontWeight.Bold)
                )
                DateButton(
                    to.toEpochSecond() * 1000,
                    formatDate,
                    onDateChanged = {
                        storeToDateTime(
                            OffsetDateTime.ofInstant(
                                Instant.ofEpochMilli(it ?: 0),
                                ZoneOffset.UTC
                            )
                        )
                    }
                )
                TimeButton(
                    to.hour,
                    to.minute,
                    formatTime,
                    onTimeChanged = { hour, minute ->
                        storeToTime(hour, minute)
                    }
                )
                Text(
                    stringResource(R.string.drink_type),
                    style = LocalTextStyle.current.copy(fontWeight = FontWeight.Bold)
                )

                FlowRow {
                    DrinkType.entries.map {
                        DrinkTypeChip(
                            onClick = { onDrinkTypeClick(it) },
                            selected = chipSelectionState[it] ?: false,
                            text = stringResource(it.description)
                        )
                    }
                }
                Button(
                    onClick = onClear,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp)
                ) {
                    Text(stringResource(R.string.clear))
                }
            }
        }
    }

    @Composable
    private fun DrinkTypeChip(
        onClick: () -> Unit,
        text: String,
        selected: Boolean,
    ) {
        FilterChip(
            modifier = Modifier.padding(end = 8.dp),
            onClick = onClick,
            label = {
                Text(text)
            },
            selected = selected,
            leadingIcon = if (selected) {
                {
                    Icon(
                        imageVector = Icons.Sharp.Done,
                        contentDescription = "Done icon",
                        modifier = Modifier.size(FilterChipDefaults.IconSize)
                    )
                }
            } else {
                null
            },
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle)

    }

    private fun onChipClick(type: DrinkType) {
        val types: Set<DrinkType> = viewModel.filter.value.drinkTypes?.let {
            if (it.contains(type)) {
                it.toMutableSet().apply {
                    remove(type)
                }
            } else {
                it.toMutableSet().apply {
                    add(type)
                }
            }
        } ?: setOf(type)
        viewModel.updateFilter { it.copy(drinkTypes = types) }
    }

    private fun storeFromDateTime(offsetDateTime: OffsetDateTime) {
        viewModel.updateFilter { it.copy(from = offsetDateTime) }
    }

    private fun storeToDateTime(offsetDateTime: OffsetDateTime) {
        viewModel.updateFilter { it.copy(to = offsetDateTime) }
    }

    private fun storeFromTime(hour: Int, minute: Int) {
        val fromDateTime = getFromDateTime()
        viewModel.updateFilter {
            it.copy(
                from = fromDateTime.withHour(hour).withMinute(minute)
            )
        }
    }

    private fun storeToTime(hour: Int, minute: Int) {
        val toDateTime = getToDateTime()
        viewModel.updateFilter { it.copy(to = toDateTime.withHour(hour).withMinute(minute)) }
    }

    private fun getToDateTime() = viewModel.filter.value.to ?: OffsetDateTime.MAX

    private fun getFromDateTime() = viewModel.filter.value.from ?: OffsetDateTime.MIN
}
