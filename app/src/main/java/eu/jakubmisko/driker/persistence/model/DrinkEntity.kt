package eu.jakubmisko.driker.persistence.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "drinks")
data class DrinkEntity(val volume: Double, val volumeUnit: String, val name: String, val type: DrinkTypeEntity, @PrimaryKey(autoGenerate = true) val id: Long = 0)

enum class DrinkTypeEntity {
    SHOT, WINE, BEER, COCKTAIL, CUSTOM
}