package eu.jakubmisko.driker.persistence.model.fts

import androidx.room.Entity
import androidx.room.Fts4
import eu.jakubmisko.driker.persistence.model.RecordEntity

@Entity(tableName = "record_fts")
@Fts4(contentEntity = RecordEntity::class)
data class RecordFts(val comment: String)