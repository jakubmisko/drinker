package eu.jakubmisko.driker.persistence.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import eu.jakubmisko.driker.persistence.model.RecordEntity
import kotlinx.coroutines.flow.Flow


@Dao
interface RecordDao {

    @Insert
    suspend fun insertRecord(recordEntity: RecordEntity): Long

    @Update
    suspend fun updateRecord(recordEntity: RecordEntity)

    @Delete
    suspend fun deleteRecord(recordEntity: RecordEntity)

    @Query("SELECT * FROM drinking_record WHERE id=:recordId")
    fun getRecord(recordId: Long): Flow<RecordEntity>

    @Query("SELECT * FROM drinking_record")
    fun getRecords(): Flow<List<RecordEntity>>

    @Query("SELECT drinking_record.drinkId, drinking_record.quantity, drinking_record.time, drinking_record.comment, drinking_record.id FROM drinking_record JOIN record_fts ON (drinking_record.id = record_fts.docid) WHERE record_fts MATCH :fulltextSearch")
    fun searchRecords(fulltextSearch: String): Flow<List<RecordEntity>>

}