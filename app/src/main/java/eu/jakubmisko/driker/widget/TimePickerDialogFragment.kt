package eu.jakubmisko.driker.widget

import android.app.Dialog
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import java.time.OffsetDateTime


class TimePickerDialogFragment : DialogFragment() {
    var timeSetListener: ((View, Int, Int) -> Unit)? = null
    var cancelListener: ((DialogInterface) -> Unit)? = null


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = OffsetDateTime.now().let {
        TimePickerDialog(
            context,
            timeSetListener,
            it.hour,
            it.minute,
            DateFormat.is24HourFormat(context)
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = OffsetDateTime.now().let {
        TimePicker(context).apply {
            setIs24HourView(DateFormat.is24HourFormat(context))
            setHours(it.hour)
            setMinutes(it.minute)
            setOnTimeChangedListener(timeSetListener)
        }
    }

    override fun onCancel(dialog: DialogInterface) {
        cancelListener?.invoke(dialog)
    }

    private fun TimePicker.setHours(hours: Int) {
        hour = hours
    }

    private fun TimePicker.setMinutes(minutes: Int) {
        minute = minutes
    }
}