package eu.jakubmisko.driker.screen

import android.widget.ImageView
import androidx.databinding.BindingAdapter

object BindingAdapters {

    @JvmStatic
    @BindingAdapter("android:src")
    fun setImageViewResource(view: ImageView, resId: Int) {
        view.setImageResource(resId)
    }
}