package eu.jakubmisko.driker.persistence.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.time.OffsetDateTime

@Entity(tableName = "drinking_record", foreignKeys = [ForeignKey(entity = DrinkEntity::class, parentColumns = ["id"], childColumns = ["drinkId"])])
data class RecordEntity(val drinkId: Long, val time: OffsetDateTime, val quantity: Int, val comment: String = "", @PrimaryKey(autoGenerate = true) val id: Long = 0)