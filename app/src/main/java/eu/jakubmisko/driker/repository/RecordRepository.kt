package eu.jakubmisko.driker.repository

import androidx.paging.PagingData
import eu.jakubmisko.driker.domain.DrinkType
import eu.jakubmisko.driker.domain.Record
import kotlinx.coroutines.flow.Flow
import java.time.OffsetDateTime

interface RecordRepository {
    suspend fun addRecord(record: Record): Long
    suspend fun updateRecord(record: Record)
    suspend fun deleteRecord(record: Record)
    suspend fun getRecord(recordId: Long): Record
    suspend fun getRecords(): List<Record>
    fun searchRecords(fulltextSearch: String): Flow<PagingData<Record>>
    fun searchRecords(drinkTypes: Set<DrinkType>): Flow<PagingData<Record>>
    fun searchRecordsFrom(dateTime: OffsetDateTime): Flow<PagingData<Record>>
    fun searchRecordsTo(dateTime: OffsetDateTime): Flow<PagingData<Record>>
    fun searchRecordsInRange(
        dateTimeFrom: OffsetDateTime,
        dateTimeTo: OffsetDateTime
    ): Flow<PagingData<Record>>

    fun searchRecordsInRange(
        dateTimeFrom: OffsetDateTime,
        dateTimeTo: OffsetDateTime,
        drinkTypes: Set<DrinkType>
    ): Flow<PagingData<Record>>

    fun getDrinkRecordsPaged(): Flow<PagingData<Record>>
}