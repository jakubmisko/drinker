package eu.jakubmisko.driker.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import eu.jakubmisko.driker.persistence.converter.DrinkTypeConverter
import eu.jakubmisko.driker.persistence.converter.OffsetDateTimeConverter
import eu.jakubmisko.driker.persistence.dao.DrinkDao
import eu.jakubmisko.driker.persistence.dao.DrinkRecordDao
import eu.jakubmisko.driker.persistence.dao.RecordDao
import eu.jakubmisko.driker.persistence.model.DrinkEntity
import eu.jakubmisko.driker.persistence.model.RecordEntity
import eu.jakubmisko.driker.persistence.model.fts.DrinkFts
import eu.jakubmisko.driker.persistence.model.fts.RecordFts

@Database(entities = [DrinkEntity::class, RecordEntity::class, DrinkFts::class, RecordFts::class], version = 1, exportSchema = false)
@TypeConverters(DrinkTypeConverter::class, OffsetDateTimeConverter::class)
abstract class Db : RoomDatabase() {
    abstract fun getDrinkDao(): DrinkDao
    abstract fun getRecordDao(): RecordDao
    abstract fun getDrinkRecordDao(): DrinkRecordDao
}