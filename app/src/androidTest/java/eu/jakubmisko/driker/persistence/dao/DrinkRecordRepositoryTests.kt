package eu.jakubmisko.driker.persistence.dao

import android.content.Context
import androidx.paging.RxPagedListBuilder
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import eu.jakubmisko.driker.persistence.Db
import eu.jakubmisko.driker.persistence.model.DrinkEntity
import eu.jakubmisko.driker.persistence.model.DrinkRecord
import eu.jakubmisko.driker.persistence.model.DrinkTypeEntity
import eu.jakubmisko.driker.persistence.model.RecordEntity
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.time.OffsetDateTime

@RunWith(AndroidJUnit4::class)
class DrinkRecordRepositoryTests {

    lateinit var db: Db

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, Db::class.java).build()
    }

    @Test
    fun fulltextJoins() {
        val drink = DrinkEntity(1.0, "ml", "poldeco", DrinkTypeEntity.SHOT, 1)
        val drink2 = DrinkEntity(1.0, "ml", "poldeco2", DrinkTypeEntity.SHOT, 2)
        val drink3 = DrinkEntity(1.0, "kg", "poldeco3", DrinkTypeEntity.SHOT, 3)
        db.getDrinkDao().insertDrink(drink).blockingGet()
        db.getDrinkDao().insertDrink(drink2).blockingGet()
        db.getDrinkDao().insertDrink(drink3).blockingGet()

        //nanos are not stored
        val record = RecordEntity(1, OffsetDateTime.now().let { it.minusNanos(it.nano.toLong()) }, 1, "kom1", 1)
        val record2 = RecordEntity(2, OffsetDateTime.now().let { it.minusNanos(it.nano.toLong()) }, 1, "kom2", 2)
        val record3 = RecordEntity(1, OffsetDateTime.now().let { it.minusNanos(it.nano.toLong()) }, 1, "ml", 3)
        val record4 = RecordEntity(3, OffsetDateTime.now().let { it.minusNanos(it.nano.toLong()) }, 2, "kom4", 4)
        db.getRecordDao().insertRecord(record).blockingGet()
        db.getRecordDao().insertRecord(record2).blockingGet()
        db.getRecordDao().insertRecord(record3).blockingGet()
        db.getRecordDao().insertRecord(record4).blockingGet()

        RxPagedListBuilder(db.getDrinkRecordDao()
                .searchRecords("ml"), 10)
                .buildObservable()
                .test()
                .apply { awaitCount(1) }
                .assertNoErrors()
                .assertValue {
                    it[0] == DrinkRecord(listOf(drink), record) && it[1] == DrinkRecord(listOf(drink), record3) && it[2] == DrinkRecord(listOf(drink2), record2)
                }
    }

    @Test
    fun update() {
        val drink = DrinkEntity(1.0, "ml", "poldeco", DrinkTypeEntity.SHOT, 1)
        db.getDrinkDao().insertDrink(drink).blockingGet()

        //nanos are not stored
        val record = RecordEntity(1, OffsetDateTime.now().let { it.minusNanos(it.nano.toLong()) }, 1, "kom1", 1)
        db.getRecordDao().insertRecord(record).blockingGet()
        val record2 = record.copy(quantity = 2, comment = "kom2")
        db.getRecordDao().updateRecord(record2).blockingAwait()
        db.getRecordDao()
                .getRecords()
                .flatMapSingle {
                    Single.just(it.firstOrNull())
                }
                .take(1)
                .test()
                .apply { awaitCount(1) }
                .assertNoErrors()
                .assertValue {
                    it.comment == "kom2" && it.quantity == 2
                }
    }
}