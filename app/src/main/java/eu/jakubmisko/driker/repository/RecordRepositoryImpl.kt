package eu.jakubmisko.driker.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.PagingSource
import androidx.paging.map
import eu.jakubmisko.driker.domain.DrinkType
import eu.jakubmisko.driker.domain.Record
import eu.jakubmisko.driker.persistence.Db
import eu.jakubmisko.driker.persistence.model.DrinkRecord
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.time.OffsetDateTime

const val PAGE_SIZE = 18

class RecordRepositoryImpl(private val db: Db) : RecordRepository {

    private fun createPagingFlow(pagingSourceFactory: () -> PagingSource<Int, DrinkRecord>): Flow<PagingData<Record>> =
        Pager(
            config = PagingConfig(
                PAGE_SIZE
            ),
            pagingSourceFactory = pagingSourceFactory
        ).flow.map {
            it.map(::domainRecord)
        }

    override suspend fun addRecord(record: Record): Long =
        db.getRecordDao().insertRecord(dbRecord(record))

    override suspend fun updateRecord(record: Record) =
        db.getRecordDao().updateRecord(dbRecord(record))

    override suspend fun deleteRecord(record: Record) =
        db.getRecordDao().deleteRecord(dbRecord(record))

    override suspend fun getRecord(recordId: Long): Record =
        db.getDrinkRecordDao().getDrinkRecord(recordId).let(::domainRecord)

    override suspend fun getRecords(): List<Record> = db.getDrinkRecordDao()
        .getDrinkRecords()
        .map(::domainRecord)

    override fun searchRecords(fulltextSearch: String): Flow<PagingData<Record>> = Pager(
        PagingConfig(
            PAGE_SIZE
        ), pagingSourceFactory = {
            db.getDrinkRecordDao()
                .searchRecords(fulltextSearch)
        }).flow
        .map {
            it.map(::domainRecord)
        }

    override fun searchRecordsFrom(dateTime: OffsetDateTime): Flow<PagingData<Record>> =
        createPagingFlow {
            db.getDrinkRecordDao().searchRecordsFrom(dateTime)
        }

    override fun searchRecordsTo(dateTime: OffsetDateTime): Flow<PagingData<Record>> =
        createPagingFlow {
            db.getDrinkRecordDao()
                .searchRecordsTo(dateTime)
        }


    override fun searchRecordsInRange(
        dateTimeFrom: OffsetDateTime,
        dateTimeTo: OffsetDateTime
    ): Flow<PagingData<Record>> = createPagingFlow {
        db.getDrinkRecordDao()
            .searchRecordsInRange(dateTimeFrom, dateTimeTo)
    }

    override fun getDrinkRecordsPaged(): Flow<PagingData<Record>> = createPagingFlow {
        db.getDrinkRecordDao()
            .getDrinkRecordsPaged()
    }

    override fun searchRecordsInRange(
        dateTimeFrom: OffsetDateTime,
        dateTimeTo: OffsetDateTime,
        drinkTypes: Set<DrinkType>
    ): Flow<PagingData<Record>> = createPagingFlow {
        db.getDrinkRecordDao()
            .searchRecordsInRangeAndType(
                dateTimeFrom,
                dateTimeTo,
                drinkTypes.map { dbDrinkType(it) }.toSet()
            )
    }

    override fun searchRecords(drinkTypes: Set<DrinkType>): Flow<PagingData<Record>> =
        createPagingFlow {
            db.getDrinkRecordDao()
                .searchRecords(drinkTypes.map { dbDrinkType(it) }.toSet())
        }
}