package eu.jakubmisko.driker.persistence.converter

import androidx.room.TypeConverter
import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneId

class OffsetDateTimeConverter {

    @TypeConverter
    fun toEpochSeconds(dateTime: OffsetDateTime): Long = dateTime.toEpochSecond()

    @TypeConverter
    fun toOffsetDateTime(epochSeconds: Long): OffsetDateTime = OffsetDateTime.ofInstant(Instant.ofEpochSecond(epochSeconds), ZoneId.systemDefault())
}