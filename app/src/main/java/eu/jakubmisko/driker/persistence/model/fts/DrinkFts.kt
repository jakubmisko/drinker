package eu.jakubmisko.driker.persistence.model.fts

import androidx.room.Entity
import androidx.room.Fts4
import eu.jakubmisko.driker.persistence.model.DrinkEntity

@Entity(tableName = "drink_fts")
@Fts4(contentEntity = DrinkEntity::class)
data class DrinkFts(val volumeUnit: String, val name: String, val type: String)