package eu.jakubmisko.driker.widget

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import java.time.OffsetDateTime

class DatePickerDialogFragment : DialogFragment() {
    lateinit var dateSetListener: (view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) -> Unit/* DatePickerDialog.OnDateSetListener*/
    var cancelListener: ((DialogInterface) -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = OffsetDateTime.now().let {

        DatePickerDialog(requireContext(), DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth -> dateSetListener(view, year, month, dayOfMonth) }, it.year, it.monthValue.dec(), it.dayOfMonth).apply {
            setOnCancelListener(cancelListener)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return DatePicker(requireContext()).also { datePicker ->
            OffsetDateTime.now().let {
                datePicker.init(it.year, it.monthValue, it.dayOfMonth) { view, year, month, dayOfMonth -> dateSetListener(view, year, month, dayOfMonth) }

            }
        }

    }

    override fun onCancel(dialog: DialogInterface) {
        cancelListener?.invoke(dialog)
    }

}