package eu.jakubmisko.driker.persistence.converter

import androidx.room.TypeConverter
import eu.jakubmisko.driker.persistence.model.DrinkTypeEntity

class DrinkTypeConverter {

    @TypeConverter
    fun toString(type: DrinkTypeEntity): String = type.name

    @TypeConverter
    fun toDrinkType(type: String): DrinkTypeEntity = DrinkTypeEntity.valueOf(type)
}