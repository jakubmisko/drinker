package eu.jakubmisko.driker.screen.history

import androidx.lifecycle.Observer
import androidx.paging.PagedList
import eu.jakubmisko.driker.DataSourceFactoryMock
import eu.jakubmisko.driker.RobolectricTest
import eu.jakubmisko.driker.domain.Drink
import eu.jakubmisko.driker.domain.DrinkType
import eu.jakubmisko.driker.domain.HistoryFilterData
import eu.jakubmisko.driker.domain.Record
import eu.jakubmisko.driker.repository.RecordRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import io.reactivex.Completable
import org.assertj.core.api.Assertions
import org.junit.Test
import java.time.OffsetDateTime
import java.util.concurrent.TimeUnit

class HistoryViewModelTest : RobolectricTest() {
    val recordRepository = mockk<RecordRepository>()

    @Test
    fun `get all records`() {
        val records = listOf(Record(Drink(0.4, "ml", "Poldeco", DrinkType.SHOT, 1), mockk(), 1, "po jednym"))
        every { recordRepository.getDrinkRecordsPaged() } returns DataSourceFactoryMock(records)
        val viewModel = HistoryViewModel(recordRepository)
        val observer: Observer<PagedList<Record>> = mockk(relaxUnitFun = true)
        viewModel.getDrinkRecords().observeForever(observer)

        val slot = slot<PagedList<Record>>()
        verify { observer.onChanged(capture(slot)) }
        Assertions.assertThat(slot.captured).containsExactly(*records.toTypedArray())
    }

    @Test
    fun `fulltext search in records`() {
        val events: MutableList<PagedList<Record>> = mutableListOf()
        val records = listOf(Record(Drink(0.4, "ml", "Poldeco", DrinkType.SHOT, 1), mockk(), 1, "po jednym"))
        val searchRecords = listOf(Record(Drink(0.5, "l", "Pivo", DrinkType.BEER, 2), mockk(), 1, ""))
        every { recordRepository.getDrinkRecordsPaged() } returns DataSourceFactoryMock(records)
        every { recordRepository.searchRecords(any<String>()) } returns DataSourceFactoryMock(searchRecords)
        val viewModel = HistoryViewModel(recordRepository)
        viewModel.getDrinkRecords().observeForever {
            events.add(it)
        }

        viewModel.search("query")

        Assertions.assertThat(events).hasSize(2)
        Assertions.assertThat(events.lastOrNull()).containsExactly(*searchRecords.toTypedArray())
    }

    @Test
    fun `search with null query`() {
        val events: MutableList<PagedList<Record>> = mutableListOf()
        val records = listOf(Record(Drink(0.4, "ml", "Poldeco", DrinkType.SHOT, 1), mockk(), 1, "po jednym"))
        every { recordRepository.getDrinkRecordsPaged() } returns DataSourceFactoryMock(records)
        val viewModel = HistoryViewModel(recordRepository)
        viewModel.getDrinkRecords().observeForever {
            events.add(it)
        }

        viewModel.search(null)

        Assertions.assertThat(events).hasSize(1)
        Assertions.assertThat(events.lastOrNull()).containsExactly(*records.toTypedArray())
    }

    @Test
    fun `apply filter with from and to range`() {
        val events: MutableList<PagedList<Record>> = mutableListOf()
        val records = listOf(Record(Drink(0.4, "ml", "Poldeco", DrinkType.SHOT, 1), mockk(), 1, "po jednym"))
        val filteredRecords = listOf(Record(Drink(0.3, "l", "Male pivo", DrinkType.BEER, 2), mockk(), 1, "kapurka"))
        every { recordRepository.getDrinkRecordsPaged() } returns DataSourceFactoryMock(records)
        every { recordRepository.searchRecordsInRange(any(), any()) } returns DataSourceFactoryMock(filteredRecords)
        val viewModel = HistoryViewModel(recordRepository)
        viewModel.getDrinkRecords().observeForever {
            events.add(it)
        }

        val from: OffsetDateTime = mockk()
        val to: OffsetDateTime = mockk()
        viewModel.filter = HistoryFilterData(from, to)
        viewModel.applyFilter()

        Assertions.assertThat(events).hasSize(2)
        Assertions.assertThat(events.lastOrNull()).contains(*filteredRecords.toTypedArray())
    }

    @Test
    fun `apply filter from date`() {
        val events: MutableList<PagedList<Record>> = mutableListOf()
        val records = listOf(Record(Drink(0.4, "ml", "Poldeco", DrinkType.SHOT, 1), mockk(), 1, "po jednym"))
        val filteredRecords = listOf(Record(Drink(0.3, "l", "Male pivo", DrinkType.BEER, 2), mockk(), 1, "kapurka"))
        every { recordRepository.getDrinkRecordsPaged() } returns DataSourceFactoryMock(records)
        every { recordRepository.searchRecordsInRange(any(), any()) } returns DataSourceFactoryMock(filteredRecords)
        val viewModel = HistoryViewModel(recordRepository)
        viewModel.getDrinkRecords().observeForever {
            events.add(it)
        }

        val from: OffsetDateTime = mockk()
        viewModel.filter = HistoryFilterData(from)
        viewModel.applyFilter()

        Assertions.assertThat(events).hasSize(2)
        Assertions.assertThat(events.lastOrNull()).contains(*filteredRecords.toTypedArray())
    }

    @Test
    fun `apply drink type filter`() {
        val events: MutableList<PagedList<Record>> = mutableListOf()
        val records = listOf(Record(Drink(0.4, "ml", "Poldeco", DrinkType.SHOT, 1), mockk(), 1, "po jednym"))
        val filteredRecords = listOf(Record(Drink(0.3, "l", "Male pivo", DrinkType.BEER, 2), mockk(), 1, "kapurka"))
        every { recordRepository.getDrinkRecordsPaged() } returns DataSourceFactoryMock(records)
        every { recordRepository.searchRecords(any<Set<DrinkType>>()) } returns DataSourceFactoryMock(filteredRecords)
        val viewModel = HistoryViewModel(recordRepository)
        viewModel.getDrinkRecords().observeForever {
            events.add(it)
        }

        val types = setOf(DrinkType.BEER)
        viewModel.filter = HistoryFilterData(drinkTypes = types)
        viewModel.applyFilter()

        Assertions.assertThat(events).hasSize(2)
        Assertions.assertThat(events.lastOrNull()).contains(*filteredRecords.toTypedArray())
    }

    @Test
    fun `apply filter with both date and drink type`() {
        val events: MutableList<PagedList<Record>> = mutableListOf()
        val records = listOf(Record(Drink(0.4, "ml", "Poldeco", DrinkType.SHOT, 1), mockk(), 1, "po jednym"))
        val filteredRecords = listOf(Record(Drink(0.3, "l", "Male pivo", DrinkType.BEER, 2), mockk(), 1, "kapurka"))
        every { recordRepository.getDrinkRecordsPaged() } returns DataSourceFactoryMock(records)
        every { recordRepository.searchRecordsInRange(any(), any(), any()) } returns DataSourceFactoryMock(filteredRecords)
        val viewModel = HistoryViewModel(recordRepository)
        viewModel.getDrinkRecords().observeForever {
            events.add(it)
        }

        val from: OffsetDateTime = mockk()
        val to: OffsetDateTime = mockk()
        val types = setOf(DrinkType.BEER)
        viewModel.filter = HistoryFilterData(from, to, types)
        viewModel.applyFilter()

        Assertions.assertThat(events).hasSize(2)
        Assertions.assertThat(events.lastOrNull()).contains(*filteredRecords.toTypedArray())
    }

    @Test
    fun `apply empty filter`() {
        val events: MutableList<PagedList<Record>> = mutableListOf()
        val records = listOf(Record(Drink(0.4, "ml", "Poldeco", DrinkType.SHOT, 1), mockk(), 1, "po jednym"))
        every { recordRepository.getDrinkRecordsPaged() } returns DataSourceFactoryMock(records)
        val viewModel = HistoryViewModel(recordRepository)
        viewModel.getDrinkRecords().observeForever {
            events.add(it)
        }

        viewModel.applyFilter()

        Assertions.assertThat(events).hasSize(1)
        Assertions.assertThat(events.lastOrNull()).contains(*records.toTypedArray())
    }

    @Test(timeout = 2000)
    fun `delete record`() {
        val events: MutableList<PagedList<Record>> = mutableListOf()
        val records = listOf(Record(Drink(0.4, "ml", "Poldeco", DrinkType.SHOT, 1), mockk(), 1, "po jednym"))
        every { recordRepository.getDrinkRecordsPaged() } returns DataSourceFactoryMock(records)
        every { recordRepository.deleteRecord(any()) } returns Completable.complete()
        val viewModel = HistoryViewModel(recordRepository)
        viewModel.getDrinkRecords().observeForever {
            events.add(it)
        }


        viewModel.delete(mockk())
                .test()
                .awaitDone(10, TimeUnit.SECONDS)
                .assertComplete()
    }

    @Test(timeout = 2000)
    fun `delete record failure`() {
        val events: MutableList<PagedList<Record>> = mutableListOf()
        val records = listOf(Record(Drink(0.4, "ml", "Poldeco", DrinkType.SHOT, 1), mockk(), 1, "po jednym"))
        every { recordRepository.getDrinkRecordsPaged() } returns DataSourceFactoryMock(records)
        every { recordRepository.deleteRecord(any()) } returns Completable.error(RuntimeException())
        val viewModel = HistoryViewModel(recordRepository)
        viewModel.getDrinkRecords().observeForever {
            events.add(it)
        }


        viewModel.delete(mockk())
                .test()
                .awaitDone(10, TimeUnit.SECONDS)
                .assertError(RuntimeException::class.java)
    }

}