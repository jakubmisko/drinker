package eu.jakubmisko.driker

import androidx.paging.DataSource
import androidx.paging.PositionalDataSource

class DataSourceFactoryMock<V>(private val data: List<V>) : DataSource.Factory<Int, V>() {
    override fun create(): DataSource<Int, V> {
        return object : PositionalDataSource<V>() {
            override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<V>) {
                params.startPosition
            }

            override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<V>) {
                val list: List<V> = when {
                    params.requestedStartPosition > data.size -> listOf()
                    params.requestedStartPosition + params.pageSize > data.size -> data
                    else -> data.subList(params.requestedStartPosition, params.requestedStartPosition + params.pageSize)
                }
                callback.onResult(list, 0, data.size)
            }

        }
    }

}