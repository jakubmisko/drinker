package eu.jakubmisko.driker.screen.history

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import eu.jakubmisko.driker.repository.RecordRepository

class HistoryViewModelFactory(private val recordRepository: RecordRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        HistoryViewModel(recordRepository) as T

}