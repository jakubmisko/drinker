package eu.jakubmisko.driker.screen.history

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import eu.jakubmisko.driker.R
import eu.jakubmisko.driker.databinding.RowHistoryBinding
import eu.jakubmisko.driker.domain.Record
import java.time.format.DateTimeFormatter

class HistoryAdapter(private val dateTimeFormatter: DateTimeFormatter, private val onItemClick: (Record) -> Unit)
    : PagedListAdapter<Record, HistoryViewHolder>(DrinkRecordDiffCallback()) {


    override fun onCreateViewHolder(view: ViewGroup, type: Int): HistoryViewHolder = HistoryViewHolder(view)


    override fun onBindViewHolder(viewHolder: HistoryViewHolder, position: Int) {
        getItem(position)?.let { record ->
            viewHolder.bind(record, dateTimeFormatter, onItemClick)
        } ?: viewHolder.clean()

    }


    fun itemToDelete(position: Int): Record? = getItem(position)
}

class HistoryViewHolder(
        parent: ViewGroup,
        private val binding: RowHistoryBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.row_history, parent, false)
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(record: Record, formatter: DateTimeFormatter, onItemClick: (Record) -> Unit) {
        binding.record = record
        binding.formatter = formatter
        binding.invalidateAll()
        itemView.setOnClickListener {
            onItemClick(record)
        }
    }

    fun clean() {
        binding.logo.setImageDrawable(null)
        binding.label.text = null
        binding.volume.text = null
        binding.dateTime.text = null
        binding.quantity.text = null
        itemView.setOnClickListener(null)
    }
}

class DrinkRecordDiffCallback : DiffUtil.ItemCallback<Record>() {
    override fun areItemsTheSame(oldItem: Record, newItem: Record): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Record, newItem: Record): Boolean = oldItem == newItem

}
