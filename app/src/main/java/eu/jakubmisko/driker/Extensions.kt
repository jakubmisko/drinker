package eu.jakubmisko.driker

import android.content.Context
import android.widget.ScrollView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun Disposable.addToDisposables(disposable: CompositeDisposable) {
    disposable.add(this)
}

fun Any.tag(): String = this::class.java.simpleName

fun Int.dpFromPx(context: Context): Float {
    return this / context.resources.displayMetrics.density
}

fun Int.pxFromDp(context: Context): Float {
    return this * context.resources.displayMetrics.density
}

fun ScrollView.setOnScrollChangeListener(onScroll: (Int, Int) -> Unit) {
    setOnScrollChangeListener { _, x, y, _, _ -> onScroll(x, y) }
}