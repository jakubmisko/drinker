package eu.jakubmisko.driker.ui

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.AccessTime
import androidx.compose.material.icons.sharp.CalendarMonth
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable

@Composable
fun TimeIcon() {
    Icon(
        imageVector = Icons.Sharp.AccessTime,
        contentDescription = "Time"
    )
}

@Composable
fun DayIcon() {
    Icon(
        imageVector = Icons.Sharp.CalendarMonth,
        contentDescription = "Day"
    )
}