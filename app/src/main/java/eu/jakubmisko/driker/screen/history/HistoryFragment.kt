package eu.jakubmisko.driker.screen.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.FilterList
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SearchBar
import androidx.compose.material3.SearchBarDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import eu.jakubmisko.driker.R
import eu.jakubmisko.driker.domain.Drink
import eu.jakubmisko.driker.domain.DrinkType
import eu.jakubmisko.driker.domain.Record
import eu.jakubmisko.driker.screen.history.row.HistoryRow
import eu.jakubmisko.driker.ui.modifiers.shimmerLoading
import kotlinx.coroutines.flow.flowOf
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

@OptIn(ExperimentalMaterial3Api::class)
class HistoryFragment : Fragment() {
    private val dateTimeFormatter: DateTimeFormatter by inject()
    private val viewModel: HistoryViewModel by viewModels(
        ownerProducer = { requireActivity() },
        factoryProducer = { get<HistoryViewModelFactory>() })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                val drinks = viewModel.getDrinkRecords().collectAsLazyPagingItems()
                HistoryPage(drinks, onSearch = { viewModel.search(it) })
            }
        }
    }

    @Preview
    @Composable
    fun HistoryPagePreview() {
        val items = flowOf(
            PagingData.from(
                listOf(
                    Record(
                        drink = Drink(
                            volume = 0.2,
                            volumeUnit = "ml",
                            name = "Coca-Cola",
                            type = DrinkType.CUSTOM,
                            id = 1
                        ),
                        time = OffsetDateTime.now(),
                        quantity = 1,
                        comment = "Comment #1",
                        id = 1,
                    ),
                    Record(
                        drink = Drink(
                            volume = 0.2,
                            volumeUnit = "ml",
                            name = "Fanta",
                            type = DrinkType.CUSTOM,
                            id = 2
                        ),
                        time = OffsetDateTime.now(),
                        quantity = 1,
                        comment = "Comment #2",
                        id = 2
                    ),
                )
            )
        ).collectAsLazyPagingItems()
        HistoryPage(items, {})
    }


    @Composable
    fun HistoryPage(records: LazyPagingItems<Record>, onSearch: (String) -> Unit) {
        var searchToggle by remember { mutableStateOf(false) }
        var queryState by remember { mutableStateOf("") }
        Scaffold(
            topBar = {
                TopAppBar(title = {
                    Text(stringResource(R.string.app_name))
                }, actions = {
                    IconButton(onClick = {
                        searchToggle = !searchToggle
                    }) {
                        Icon(Icons.Filled.Search, contentDescription = "Search")
                    }
                    IconButton(onClick = {
                        findNavController().navigate(HistoryFragmentDirections.actionHistoryFragmentToHistoryFilter())
                    }) {
                        Icon(Icons.Filled.FilterList, contentDescription = "Filter")
                    }
                })
            },
            floatingActionButton = {
                FloatingActionButton(onClick = {
                    findNavController().navigate(HistoryFragmentDirections.addEditDrinkRecord())
                }) {
                    Icon(Icons.Filled.Add, contentDescription = "Add")
                }
            }) { padding ->
            if (searchToggle) {
                Column(Modifier.padding(padding)) {
                    SearchBar(
                        modifier = Modifier.padding(start = 16.dp),
                        inputField = {
                            SearchBarDefaults.InputField(
                                query = queryState,
                                onSearch = onSearch,
                                onQueryChange = { queryState = it },
                                expanded = false,
                                onExpandedChange = {},
                                placeholder = { Text(stringResource(R.string.search)) },
                                leadingIcon = {
                                    Icon(
                                        Icons.Default.Search,
                                        contentDescription = "Search"
                                    )
                                },
                                trailingIcon = {
                                    IconButton(onClick = { queryState = "" }) {
                                        Icon(
                                            Icons.Default.Close,
                                            contentDescription = "Close"
                                        )
                                    }
                                },
                            )
                        }, expanded = false,
                        onExpandedChange = {}
                    ) {

                    }
                    RecordsList(PaddingValues(), records)
                }
            } else {
                RecordsList(padding, records)
            }
        }
    }

    @Composable
    private fun RecordsList(
        padding: PaddingValues,
        records: LazyPagingItems<Record>
    ) {
        LazyColumn(modifier = Modifier.padding(padding)) {
            items(
                count = records.itemCount,
                key = { index ->
                    records[index]?.id?.toInt()
                        ?: throw IllegalStateException("Id should not be null")
                }
            ) { index ->
                records[index]?.let {
                    HistoryRow(
                        record = it,
                        dateTimeFormatter = dateTimeFormatter,
                        onClick = { record ->
                            findNavController().navigate(
                                HistoryFragmentDirections.addEditDrinkRecord()
                                    .setEditDrink(record)
                            )
                        })
                } ?: LoadingRow()
            }
            records.apply {
                when {
                    loadState.refresh is LoadState.Loading -> {
                        item { LoadingRow() }
                    }

                    loadState.refresh is LoadState.Error -> {
                        val error = loadState.refresh as LoadState.Error
                        item {
                            ErrorRow(
                                errorMessage = error.error.message ?: "Unknown error",
                                onRetry = { records.retry() },
                            )
                        }
                    }

                    loadState.append is LoadState.Loading -> {
                        item { LoadingRow() }
                    }

                    loadState.append is LoadState.Error -> {
                        val error = loadState.append as LoadState.Error
                        item {
                            ErrorRow(
                                errorMessage = error.error.message ?: "Unknown error",
                                onRetry = { records.retry() },
                            )
                        }
                    }
                }
            }
        }
    }

    @Preview
    @Composable
    fun ErrorRowPreview() {
        ErrorRow(errorMessage = "Error", onRetry = {})
    }

    @Composable
    private fun ErrorRow(
        errorMessage: String,
        onRetry: () -> Unit
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = painterResource(R.drawable.ic_error_outline_24), "Error",
                modifier = Modifier.size(56.dp)
            )
            Spacer(Modifier.padding(start = 8.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(errorMessage)
                TextButton(onRetry) {
                    Text(stringResource(R.string.retry))
                }
            }
        }
    }

    @Preview
    @Composable
    fun LoadingRowPreview() {
        LoadingRow()
    }

    @Composable
    private fun LoadingRow() {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Box(
                modifier = Modifier
                    .size(56.dp)
                    .shimmerLoading()
            )
            Column(Modifier.padding(start = 8.dp)) {
                Box(
                    Modifier
                        .fillMaxWidth()
                        .height(16.dp)
                        .shimmerLoading()
                )
                Spacer(Modifier.height(2.dp))
                Box(
                    Modifier
                        .fillMaxWidth()
                        .height(16.dp)
                        .shimmerLoading()
                )
            }
        }
    }
}