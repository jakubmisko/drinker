package eu.jakubmisko.driker.repository

import eu.jakubmisko.driker.domain.Drink
import eu.jakubmisko.driker.domain.DrinkType
import eu.jakubmisko.driker.domain.Record
import eu.jakubmisko.driker.persistence.model.DrinkEntity
import eu.jakubmisko.driker.persistence.model.DrinkRecord
import eu.jakubmisko.driker.persistence.model.RecordEntity

fun domainDrink(drink: DrinkEntity): Drink = Drink(drink.volume, drink.volumeUnit, drink.name, domainDrinkType(drink.type), drink.id)

fun dbDrink(drink: Drink): DrinkEntity = DrinkEntity(drink.volume, drink.volumeUnit, drink.name, dbDrinkType(drink.type), drink.id)

fun domainRecord(drinkRecord: DrinkRecord): Record = Record(domainDrink(drinkRecord.drinks[0]), drinkRecord.record.time, drinkRecord.record.quantity, drinkRecord.record.comment, drinkRecord.record.id)

fun dbRecord(record: Record): RecordEntity = RecordEntity(record.drink.id, record.time, record.quantity, record.comment, record.id)

fun domainDrinkType(drinkType: eu.jakubmisko.driker.persistence.model.DrinkTypeEntity) = DrinkType.valueOf(drinkType.toString())

fun dbDrinkType(drinkType: DrinkType) = eu.jakubmisko.driker.persistence.model.DrinkTypeEntity.valueOf(drinkType.toString())