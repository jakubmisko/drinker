package eu.jakubmisko.driker.screen.add

import androidx.lifecycle.Observer
import eu.jakubmisko.driker.RobolectricTest
import eu.jakubmisko.driker.domain.Drink
import eu.jakubmisko.driker.domain.DrinkType
import eu.jakubmisko.driker.domain.Result
import eu.jakubmisko.driker.domain.ResultFactory
import eu.jakubmisko.driker.repository.DrinkRepository
import eu.jakubmisko.driker.repository.RecordRepository
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import io.mockk.slot
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import org.assertj.core.api.Assertions
import org.junit.Test
import java.time.OffsetDateTime

class AddEditRecordViewModelTests : RobolectricTest() {
    private val drinkRepository: DrinkRepository = mockk()
    private val recordRepository: RecordRepository = mockk()

    @Test
    fun `get drinks`() {
        val data = listOf(Drink(0.5, "l", "Pivo", DrinkType.BEER, 1))
        every { drinkRepository.getAllDrinks() } returns Observable.just(data)
        val viewModel = AddEditRecordViewModel(drinkRepository, recordRepository)
        val slot = slot<Result<List<Drink>>>()
        val observer: Observer<Result<List<Drink>>> = mockk()
        every { observer.onChanged(capture(slot)) } just runs
        viewModel.loadAvailableDrinks()
                .observeForever(observer)

        verify { observer.onChanged(any()) }
        Assertions.assertThat(slot.captured).isEqualTo(ResultFactory.success(data))
    }

    @Test
    fun `save empty record`() {
        val viewModel = AddEditRecordViewModel(drinkRepository, recordRepository)

        viewModel.persist()


        Assertions.assertThat(viewModel.events.observeBlocking()).describedAs("Add result")
                .isInstanceOf(Result.Failure::class.java)
                .matches {
                    it as Result.Failure
                    it.throwable is IllegalArgumentException && it.throwable.message?.equals("Drink is not set") == true
                }
    }

    @Test
    fun `save proper record`() {
        every { recordRepository.addRecord(any()) } returns Single.just(1)
        val viewModel = AddEditRecordViewModel(drinkRepository, recordRepository)
        viewModel.comment = "comento"
        viewModel.dateTime = OffsetDateTime.MIN
        viewModel.drink = Drink(0.5, "l", "Pivo", DrinkType.BEER, 1)
        viewModel.quantity = 1

        viewModel.persist()

        Assertions.assertThat(viewModel.events.observeBlocking()).describedAs("Add result").isEqualTo(ResultFactory.completed())

    }

    @Test
    fun `save error from repository`() {
        val message = "Unable to persist record."
        every { recordRepository.addRecord(any()) } returns Single.error(RuntimeException(message))
        val viewModel = AddEditRecordViewModel(drinkRepository, recordRepository)
        viewModel.comment = "comento"
        viewModel.dateTime = OffsetDateTime.MIN
        viewModel.drink = Drink(0.5, "l", "Pivo", DrinkType.BEER, 1)
        viewModel.quantity = 1

        viewModel.persist()

        Assertions.assertThat(viewModel.events.observeBlocking()).describedAs("Add result").isInstanceOf(Result.Failure::class.java)
                .matches {
                    it as Result.Failure
                    it.throwable is RuntimeException && it.throwable.message?.equals(message) == true
                }
    }

    @Test
    fun `edit with empty record`() {
        val message = "Drink is not set"
        val viewModel = AddEditRecordViewModel(drinkRepository, recordRepository)
        viewModel.recordId = 1

        viewModel.persist()

        Assertions.assertThat(viewModel.events.observeBlocking()).describedAs("Edit result").isInstanceOf(Result.Failure::class.java)
                .matches {
                    it as Result.Failure
                    it.throwable is IllegalArgumentException && it.throwable.message?.equals("Drink is not set") == true
                }
    }

    @Test
    fun `edit record with proper fields`() {
        every { recordRepository.updateRecord(any()) } returns Completable.complete()
        val viewModel = AddEditRecordViewModel(drinkRepository, recordRepository)
        viewModel.comment = "comento"
        viewModel.dateTime = OffsetDateTime.MIN
        viewModel.drink = Drink(0.5, "l", "Pivo", DrinkType.BEER, 1)
        viewModel.quantity = 1
        viewModel.recordId = 1

        viewModel.persist()

        Assertions.assertThat(viewModel.events.observeBlocking()).describedAs("Edit result").isEqualTo(ResultFactory.completed())
    }

    @Test
    fun `edit record error from repository`() {
        val message = "Unable to persist record."
        every { recordRepository.updateRecord(any()) } returns Completable.error(RuntimeException(message))
        val viewModel = AddEditRecordViewModel(drinkRepository, recordRepository)
        viewModel.comment = "comento"
        viewModel.dateTime = OffsetDateTime.MIN
        viewModel.drink = Drink(0.5, "l", "Pivo", DrinkType.BEER, 1)
        viewModel.quantity = 1
        viewModel.recordId = 1

        viewModel.persist()

        Assertions.assertThat(viewModel.events.observeBlocking()).describedAs("Edit result").isInstanceOf(Result.Failure::class.java)
                .matches {
                    it as Result.Failure
                    it.throwable is RuntimeException && it.throwable.message?.equals(message) == true
                }
    }


}