package eu.jakubmisko.driker.screen.add

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import eu.jakubmisko.driker.R
import org.hamcrest.CoreMatchers
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.KoinTest
import java.time.OffsetDateTime


@RunWith(AndroidJUnit4::class)
class AddEditRecordFragmentTest : KoinTest {
//    private val db = mockk<Db>()
//
//    @Before
//    fun before() {
//
//        startKoin {
//            modules(module {
//                single { db }
//            })
//        }
//    }

    @Test
    fun selectDrink() {
        val now = OffsetDateTime.now()
        val fragmentScenario = launchFragmentInContainer<AddEditRecordFragment>(themeResId = R.style.AppTheme, fragmentArgs = Bundle())

        Espresso.onView(ViewMatchers.withId(R.id.drink)).perform(ViewActions.typeText("Beer 12 (0.5"))
        Espresso.onData(Matchers.hasToString("Beer 12 (0.5")).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.comment)).perform(ViewActions.typeText("drink #1"))
        Espresso.onView(ViewMatchers.withId(R.id.comment)).perform(ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.plus)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.countText)).check(ViewAssertions.matches(ViewMatchers.withText("2")))
        Espresso.onView(ViewMatchers.withId(R.id.addRecord)).perform(ViewActions.click())

        fragmentScenario.onFragment {
            Assert.assertThat("fragmen is detached", it.lifecycle.currentState, CoreMatchers.equalTo(Lifecycle.State.DESTROYED))
        }
    }
}