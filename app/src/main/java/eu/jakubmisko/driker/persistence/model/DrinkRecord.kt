package eu.jakubmisko.driker.persistence.model

import androidx.room.Embedded
import androidx.room.Relation


data class DrinkRecord(
        @Relation(parentColumn = "drinkId", entityColumn = "id")
        val drinks: List<DrinkEntity>,
        @Embedded
        val record: RecordEntity
)