package eu.jakubmisko.driker.screen.history

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import eu.jakubmisko.driker.R
import org.hamcrest.Matchers
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class HistoryFragmentTests {
//    private val repo = mockk<RecordRepository>()
//
//    @Before
//    fun before() {
//
//        startKoin {
//            modules(module {
//                single { repo }
//            })
//        }
//    }

    @Test
    fun checkListOfRecords() {
//        every { repo.getDrinkRecordsPaged().toLiveData(any<Int>()) } returns
        val fragmentScenario = launchFragmentInContainer<HistoryFragment>(themeResId = R.style.AppTheme)

        Espresso.onData(ViewMatchers.withId(R.id.label)).check(ViewAssertions.matches(ViewMatchers.withText(Matchers.not(Matchers.isEmptyOrNullString()))))
    }
}