package eu.jakubmisko.driker.persistence.migrations

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import eu.jakubmisko.driker.persistence.model.DrinkTypeEntity

object Initialization : RoomDatabase.Callback() {
    override fun onCreate(db: SupportSQLiteDatabase) {
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Vodka", 0.4, "ml", DrinkTypeEntity.SHOT))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Rum", 0.4, "ml", DrinkTypeEntity.SHOT))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Gin", 0.4, "ml", DrinkTypeEntity.SHOT))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Cognac", 0.4, "ml", DrinkTypeEntity.SHOT))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Tequila", 0.4, "ml", DrinkTypeEntity.SHOT))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Liqueur", 0.4, "ml", DrinkTypeEntity.SHOT))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Whiskey", 0.4, "ml", DrinkTypeEntity.SHOT))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Red wine", 0.2, "l", DrinkTypeEntity.WINE))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("White wine", 0.2, "l", DrinkTypeEntity.WINE))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Rose wine", 0.2, "l", DrinkTypeEntity.WINE))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Beer 12", 0.5, "l", DrinkTypeEntity.BEER))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Beer 12", 0.3, "l", DrinkTypeEntity.BEER))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Beer 10", 0.5, "l", DrinkTypeEntity.BEER))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Beer 10", 0.3, "l", DrinkTypeEntity.BEER))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Margarita", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Cosmopolitan", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Negroni", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Moscow mule", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Mojito", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Whiskey sour", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Manhatan", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Mimosa", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Gimlet", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Sazerac", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Pimm's cup", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Paloma", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Martini", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Daiquiri", 0.2, "l", DrinkTypeEntity.COCKTAIL))
        db.insert("drinks", SQLiteDatabase.CONFLICT_ABORT, createDrink("Long island ice tea", 0.2, "l", DrinkTypeEntity.COCKTAIL))

        db.execSQL("INSERT INTO record_fts(record_fts) VALUES('rebuild');")
        db.execSQL("INSERT INTO drink_fts(drink_fts) VALUES('rebuild');")
    }

    private fun createDrink(name: String, volume: Double, volumeUnit: String, drinkType: DrinkTypeEntity): ContentValues {
        return ContentValues().apply {
            put("volume", volume)
            put("volumeUnit", volumeUnit)
            put("name", name)
            put("type", drinkType.toString())
        }
    }
}