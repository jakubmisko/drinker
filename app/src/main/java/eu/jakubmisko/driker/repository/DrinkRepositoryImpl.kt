package eu.jakubmisko.driker.repository

import eu.jakubmisko.driker.domain.Drink
import eu.jakubmisko.driker.persistence.Db
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class DrinkRepositoryImpl(private val db: Db) : DrinkRepository {

    override suspend fun addDrink(drink: Drink): Long = db.getDrinkDao().insertDrink(dbDrink(drink))

    override suspend fun updateDrink(drink: Drink) = db.getDrinkDao().updateDrink(dbDrink(drink))

    override suspend fun deleteDrink(drink: Drink) = db.getDrinkDao().deleteDrink(dbDrink(drink))

    override fun getDrink(drinkId: Long): Flow<Drink> =
        db.getDrinkDao().getDrink(drinkId).map(::domainDrink)

    override fun getAllDrinks(): Flow<List<Drink>> =
        db.getDrinkDao().getAllDrinks().map { it.map(::domainDrink) }

    override fun searchDrinks(fulltextSearch: String): Flow<List<Drink>> =
        db.getDrinkDao().searchDrinks(fulltextSearch).map { it.map(::domainDrink) }

}