package eu.jakubmisko.driker.ui.popup

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Close
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.BottomEnd
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Alignment.Companion.TopStart
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Popup
import eu.jakubmisko.driker.R

@Composable
@Preview
fun PopupPreview() {
    DateTimePopup({}, {}, "Title") {
        Text("Preview")
    }
}

@Composable
fun DateTimePopup(
    onDismiss: () -> Unit,
    onApply: () -> Unit,
    title: String,
    content: @Composable () -> Unit
) {
    Popup(
        onDismissRequest = onDismiss,
        alignment = TopStart
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .shadow(elevation = 4.dp)
                .background(MaterialTheme.colorScheme.surface)
        ) {
            Row {
                IconButton(onDismiss) {
                    Icon(
                        imageVector = Icons.Sharp.Close,
                        contentDescription = "Close"
                    )
                }
                Text(
                    title,
                    modifier = Modifier.align(CenterVertically),
                    style = MaterialTheme.typography.titleSmall,
                )
            }

            content()
            Box(
                Modifier
                    .padding(16.dp)
                    .fillMaxWidth(), contentAlignment = BottomEnd
            ) {
                Button(onClick = {
                    onApply()
                    onDismiss()
                }) {
                    Text(stringResource(R.string.apply))
                }
            }
        }
    }
}