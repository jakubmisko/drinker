package eu.jakubmisko.driker

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.After
import org.junit.Rule
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.robolectric.annotation.Config
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(AndroidJUnit4::class)
abstract class RobolectricTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @After
    fun tearDown() {
        stopKoin()
    }

    fun <T> LiveData<T>.observeBlocking(): T {
        val latch = CountDownLatch(1)
        var value: T? = null
        this.observeForever {
            latch.countDown()
            value = it
        }
        latch.await(1, TimeUnit.MINUTES)
        return value ?: throw IllegalStateException("Value not present")
    }
}