package eu.jakubmisko.driker.repository

import eu.jakubmisko.driker.domain.Drink
import kotlinx.coroutines.flow.Flow

interface DrinkRepository {
    suspend fun addDrink(drink: Drink): Long

    suspend fun updateDrink(drink: Drink)

    suspend fun deleteDrink(drink: Drink)

    fun getDrink(drinkId: Long): Flow<Drink>

    fun getAllDrinks(): Flow<List<Drink>>

    fun searchDrinks(fulltextSearch: String): Flow<List<Drink>>
}