package eu.jakubmisko.driker.domain

import io.reactivex.FlowableTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer

object ResultFactory {
    fun completed(): Result<Unit> = Result.Success(Unit)
    fun <T> success(data: T): Result<T> = Result.Success(data)
    fun <T> failure(throwable: Throwable): Result<T> = Result.Failure(throwable)
}

fun <T> Result<T>.isSuccess(): Result.Success<T>? {
    return this.takeIf { it is Result.Success<T> } as Result.Success<T>?
}

fun <T> Result<T>.isFailure(): Result.Failure<T>? {
    return this.takeIf { it is Result.Failure<T> } as Result.Failure<T>?
}

fun <T> flowableToResult(): FlowableTransformer<T, Result<T>> {
    return FlowableTransformer {
        it.map<Result<T>> { ResultFactory.success(it) }
                .onErrorReturn { ResultFactory.failure(it) }
    }
}

fun <T> singleToResult(): SingleTransformer<T, Result<T>> {
    return SingleTransformer {
        it.map<Result<T>> { ResultFactory.success(it) }
                .onErrorReturn { ResultFactory.failure(it) }
    }
}

fun <T> observableToResult(): ObservableTransformer<T, Result<T>> {
    return ObservableTransformer {
        it.map<Result<T>> { ResultFactory.success(it) }
                .onErrorReturn { ResultFactory.failure(it) }
    }
}