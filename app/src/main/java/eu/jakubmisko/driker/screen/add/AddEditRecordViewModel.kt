package eu.jakubmisko.driker.screen.add

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import eu.jakubmisko.driker.domain.Drink
import eu.jakubmisko.driker.domain.Record
import eu.jakubmisko.driker.domain.Result
import eu.jakubmisko.driker.domain.ResultFactory
import eu.jakubmisko.driker.repository.DrinkRepository
import eu.jakubmisko.driker.repository.RecordRepository
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor

data class RecordState(
    val drink: Drink? = null,
    val dateTime: OffsetDateTime = OffsetDateTime.now(),
    val quantity: Int? = 1,
    val comment: String = "",
    val recordId: Long? = null
)

class AddEditRecordViewModel(
    private val drinkRepository: DrinkRepository,
    private val recordRepository: RecordRepository,
    private val dateFormatter: DateTimeFormatter,
    private val timeFormatter: DateTimeFormatter,
) : ViewModel() {
    private val _state = MutableStateFlow(RecordState())
    val state = _state.asStateFlow()
    private val _events = MutableSharedFlow<Result<Unit>>(
        extraBufferCapacity = 1,
        onBufferOverflow = BufferOverflow.DROP_OLDEST
    )
    val events: SharedFlow<Result<Unit>> = _events.asSharedFlow()
    val availableDrinks = drinkRepository
        .getAllDrinks()
        .stateIn(viewModelScope, SharingStarted.Lazily, emptyList())

    fun updateState(update: (RecordState) -> RecordState) {
        _state.update(update)
    }

    fun persist() {
        viewModelScope.launch {
            val result = kotlin.runCatching {
                state.value.run {
                    requireNotNull(drink) { "Drink is not set" }
                    requireNotNull(quantity) { "Quantity is not set" }

                    if (recordId != null)
                        recordRepository.updateRecord(
                            Record(
                                drink,
                                dateTime,
                                quantity,
                                comment,
                                recordId
                            )
                        )
                    else recordRepository.addRecord(
                        Record(
                            drink,
                            dateTime,
                            quantity,
                            comment
                        )
                    )
                }
            }.fold(
                onSuccess = { ResultFactory.completed() }, onFailure = {
                    ResultFactory.failure(it)
                })
        }
    }

    fun formatDate(date: TemporalAccessor): String = dateFormatter.format(date)

    fun formatTime(time: TemporalAccessor): String = timeFormatter.format(time)

}