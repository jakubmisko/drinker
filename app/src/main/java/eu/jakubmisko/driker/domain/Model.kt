package eu.jakubmisko.driker.domain

import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import eu.jakubmisko.driker.R
import kotlinx.parcelize.Parcelize
import java.time.OffsetDateTime

@Parcelize
data class Drink(
    val volume: Double,
    val volumeUnit: String,
    val name: String,
    val type: DrinkType,
    val id: Long = 0
) : Parcelable {
    override fun toString(): String = "$name ($volume$volumeUnit)"
}

enum class DrinkType(@DrawableRes val image: Int, @StringRes val description: Int) {
    SHOT(R.drawable.ic_shot_glass, R.string.shot),
    WINE(R.drawable.ic_wine, R.string.wine),
    BEER(R.drawable.ic_beer, R.string.beer),
    COCKTAIL(R.drawable.ic_cocktail, R.string.cocktail),
    CUSTOM(R.drawable.ic_question_mark, R.string.mysterious)
}

@Parcelize
data class Record(
    val drink: Drink,
    val time: OffsetDateTime,
    val quantity: Int,
    val comment: String,
    val id: Long = 0
) : Parcelable

@Parcelize
data class HistoryFilterData(
    val from: OffsetDateTime? = null,
    val to: OffsetDateTime? = null,
    val drinkTypes: Set<DrinkType>? = null
) : Parcelable

sealed class Result<T> {
    data class Success<T>(val data: T) : Result<T>()
    data class Failure<T>(val throwable: Throwable) : Result<T>()
}