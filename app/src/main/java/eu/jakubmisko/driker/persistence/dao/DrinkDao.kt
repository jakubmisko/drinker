package eu.jakubmisko.driker.persistence.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import eu.jakubmisko.driker.persistence.model.DrinkEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface DrinkDao {

    @Insert
    suspend fun insertDrink(drinkEntity: DrinkEntity): Long

    @Update
    suspend fun updateDrink(drinkEntity: DrinkEntity)

    @Delete
    suspend fun deleteDrink(drinkEntity: DrinkEntity)

    @Query("SELECT * FROM drinks WHERE id=:drinkId")
    fun getDrink(drinkId: Long): Flow<DrinkEntity>

    @Query("SELECT * FROM drinks")
    fun getAllDrinks(): Flow<List<DrinkEntity>>

    @Query("SELECT drinks.volume, drinks.volumeUnit, drinks.name, drinks.type, drinks.id FROM drinks JOIN drink_fts ON (drinks.id = drink_fts.docid) WHERE drink_fts MATCH :fulltextSearch")
    fun searchDrinks(fulltextSearch: String): Flow<List<DrinkEntity>>

}