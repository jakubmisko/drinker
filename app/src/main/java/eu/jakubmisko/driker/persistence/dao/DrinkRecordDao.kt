package eu.jakubmisko.driker.persistence.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import eu.jakubmisko.driker.persistence.model.DrinkRecord
import eu.jakubmisko.driker.persistence.model.DrinkTypeEntity
import java.time.OffsetDateTime

@Dao
interface DrinkRecordDao {

    @Transaction
    @Query("SELECT * from drinking_record")
    suspend fun getDrinkRecords(): List<DrinkRecord>

    @Transaction
    @Query("SELECT * from drinking_record WHERE id=:recordId")
    suspend fun getDrinkRecord(recordId: Long): DrinkRecord

    @Transaction
    @Query("SELECT * from drinking_record ORDER BY time DESC")
    fun getDrinkRecordsPaged(): PagingSource<Int, DrinkRecord>

    @Transaction
    @Query("SELECT drinking_record.drinkId, drinking_record.time, drinking_record.quantity, drinking_record.comment, drinking_record.id " +
            "FROM drinking_record JOIN drink_fts ON (drinking_record.drinkId = drink_fts.docid) WHERE drink_fts MATCH :fulltext " +
            "UNION " +
            "SELECT drinking_record.drinkId, drinking_record.time, drinking_record.quantity, drinking_record.comment, drinking_record.id " +
            "FROM drinking_record JOIN record_fts ON (drinking_record.id = record_fts.docid) WHERE record_fts MATCH :fulltext")
    fun searchRecords(fulltext: String): PagingSource<Int, DrinkRecord>

    @Transaction
    @Query("SELECT * FROM drinking_record WHERE time > :dateTime")
    fun searchRecordsFrom(dateTime: OffsetDateTime): PagingSource<Int, DrinkRecord>

    @Transaction
    @Query("SELECT * FROM drinking_record WHERE time < :dateTime")
    fun searchRecordsTo(dateTime: OffsetDateTime): PagingSource<Int, DrinkRecord>

    @Transaction
    @Query("SELECT * FROM drinking_record WHERE time BETWEEN :dateTimeFrom AND :dateTimeTo")
    fun searchRecordsInRange(
        dateTimeFrom: OffsetDateTime,
        dateTimeTo: OffsetDateTime
    ): PagingSource<Int, DrinkRecord>

    @Transaction
    @Query("SELECT * FROM drinking_record as dr JOIN drinks as d ON d.id = dr.drinkId WHERE time BETWEEN :dateTimeFrom AND :dateTimeTo AND d.type IN (:drinkTypes)")
    fun searchRecordsInRangeAndType(
        dateTimeFrom: OffsetDateTime,
        dateTimeTo: OffsetDateTime,
        drinkTypes: Set<DrinkTypeEntity>
    ): PagingSource<Int, DrinkRecord>

    @Transaction
    @Query("SELECT * FROM drinking_record as dr JOIN drinks as d ON d.id = dr.drinkId WHERE d.type IN (:drinkTypes)")
    fun searchRecords(drinkTypes: Set<DrinkTypeEntity>): PagingSource<Int, DrinkRecord>
}