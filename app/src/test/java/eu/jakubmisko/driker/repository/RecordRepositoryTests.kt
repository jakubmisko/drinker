package eu.jakubmisko.driker.repository

import androidx.lifecycle.Observer
import androidx.paging.PagedList
import androidx.paging.toLiveData
import eu.jakubmisko.driker.DataSourceFactoryMock
import eu.jakubmisko.driker.RobolectricTest
import eu.jakubmisko.driker.domain.Drink
import eu.jakubmisko.driker.domain.DrinkType
import eu.jakubmisko.driker.domain.Record
import eu.jakubmisko.driker.persistence.Db
import eu.jakubmisko.driker.persistence.model.DrinkEntity
import eu.jakubmisko.driker.persistence.model.DrinkRecord
import eu.jakubmisko.driker.persistence.model.DrinkTypeEntity
import eu.jakubmisko.driker.persistence.model.RecordEntity
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import io.mockk.slot
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import org.assertj.core.api.Assertions
import org.junit.Test
import java.time.OffsetDateTime

class RecordRepositoryTests : RobolectricTest() {
    private val db: Db = mockk()
    private val repo: RecordRepository = RecordRepositoryImpl(db)
    private val record = Record(Drink(0.5, "l", "Pivo", DrinkType.BEER, 1), OffsetDateTime.MIN, 1, "comment", 1)
    private val recordDb = DrinkRecord(listOf(DrinkEntity(0.5, "l", "Pivo", DrinkTypeEntity.BEER, 1)), RecordEntity(1, OffsetDateTime.MIN, 1, "comment", 1))


    @Test
    fun `add record`() {
        every { db.getRecordDao().insertRecord(any()) } returns Single.just(1)

        repo.addRecord(record)
                .test()
                .apply { awaitTerminalEvent() }
                .assertNoErrors()
                .assertComplete()
                .assertValue(1)

        verify(exactly = 1) { db.getRecordDao().insertRecord(any()) }
    }

    @Test
    fun `edit record`() {
        every { db.getRecordDao().updateRecord(any()) } returns Completable.complete()

        repo.updateRecord(record)
                .test()
                .apply { awaitTerminalEvent() }
                .assertNoErrors()
                .assertComplete()

        verify(exactly = 1) { db.getRecordDao().updateRecord(any()) }
    }

    @Test
    fun `delete record`() {
        every { db.getRecordDao().deleteRecord(any()) } returns Completable.complete()

        repo.deleteRecord(record)
                .test()
                .apply { awaitTerminalEvent() }
                .assertNoErrors()
                .assertComplete()

        verify(exactly = 1) { db.getRecordDao().deleteRecord(any()) }
    }

    @Test
    fun `get record by id`() {

        every { db.getDrinkRecordDao().getDrinkRecord(any()) } returns Observable.just(recordDb)

        repo.getRecord(1)
                .test()
                .apply { awaitTerminalEvent() }
                .assertNoErrors()
                .assertComplete()
                .assertValue(record)

        verify(exactly = 1) { db.getDrinkRecordDao().getDrinkRecord(any()) }
    }

    @Test
    fun `get records`() {
        every { db.getDrinkRecordDao().getDrinkRecords() } returns Observable.just(listOf(recordDb))

        repo.getRecords()
                .test()
                .apply { awaitTerminalEvent() }
                .assertNoErrors()
                .assertComplete()
                .assertValue(listOf(record))

        verify(exactly = 1) { db.getDrinkRecordDao().getDrinkRecords() }
    }

    @Test
    fun `get records paged`() {
        every { db.getDrinkRecordDao().getDrinkRecordsPaged() } returns DataSourceFactoryMock(listOf(recordDb))
        val slot = slot<PagedList<Record>>()
        val observer: Observer<PagedList<Record>> = mockk()
        every { observer.onChanged(capture(slot)) } just runs

        repo.getDrinkRecordsPaged()
                .toLiveData(1)
                .observeForever(observer)

        Assertions.assertThat(slot.captured).contains(*listOf(record).toTypedArray())
        verify(exactly = 1) { observer.onChanged(any()) }
        verify(exactly = 1) { db.getDrinkRecordDao().getDrinkRecordsPaged() }
    }

    @Test
    fun `fulltext search records`() {
        every { db.getDrinkRecordDao().searchRecords(any<String>()) } returns DataSourceFactoryMock(listOf(recordDb))
        val slot = slot<PagedList<Record>>()
        val observer: Observer<PagedList<Record>> = mockk()
        every { observer.onChanged(capture(slot)) } just runs

        repo.searchRecords("query")
                .toLiveData(1)
                .observeForever(observer)

        Assertions.assertThat(slot.captured).contains(*listOf(record).toTypedArray())
        verify(exactly = 1) { observer.onChanged(any()) }
        verify(exactly = 1) { db.getDrinkRecordDao().searchRecords(any<String>()) }
    }

    @Test
    fun `search records by drink type`() {
        every { db.getDrinkRecordDao().searchRecords(any<Set<DrinkTypeEntity>>()) } returns DataSourceFactoryMock(listOf(recordDb))
        val slot = slot<PagedList<Record>>()
        val observer: Observer<PagedList<Record>> = mockk()
        every { observer.onChanged(capture(slot)) } just runs

        repo.searchRecords(setOf(DrinkType.BEER))
                .toLiveData(1)
                .observeForever(observer)

        Assertions.assertThat(slot.captured).contains(*listOf(record).toTypedArray())
        verify(exactly = 1) { observer.onChanged(any()) }
        verify(exactly = 1) { db.getDrinkRecordDao().searchRecords(any<Set<DrinkTypeEntity>>()) }
    }

    @Test
    fun `search records from date`() {
        every { db.getDrinkRecordDao().searchRecordsFrom(any()) } returns DataSourceFactoryMock(listOf(recordDb))
        val slot = slot<PagedList<Record>>()
        val observer: Observer<PagedList<Record>> = mockk()
        every { observer.onChanged(capture(slot)) } just runs

        repo.searchRecordsFrom(OffsetDateTime.MIN)
                .toLiveData(1)
                .observeForever(observer)

        Assertions.assertThat(slot.captured).contains(*listOf(record).toTypedArray())
        verify(exactly = 1) { observer.onChanged(any()) }
        verify(exactly = 1) { db.getDrinkRecordDao().searchRecordsFrom(any()) }
    }

    @Test
    fun `search records to date`() {
        every { db.getDrinkRecordDao().searchRecordsTo(any()) } returns DataSourceFactoryMock(listOf(recordDb))
        val slot = slot<PagedList<Record>>()
        val observer: Observer<PagedList<Record>> = mockk()
        every { observer.onChanged(capture(slot)) } just runs

        repo.searchRecordsTo(OffsetDateTime.MIN)
                .toLiveData(1)
                .observeForever(observer)

        Assertions.assertThat(slot.captured).contains(*listOf(record).toTypedArray())
        verify(exactly = 1) { observer.onChanged(any()) }
        verify(exactly = 1) { db.getDrinkRecordDao().searchRecordsTo(any()) }
    }

    @Test
    fun `search records in date range`() {
        every { db.getDrinkRecordDao().searchRecordsInRange(any(), any()) } returns DataSourceFactoryMock(listOf(recordDb))
        val slot = slot<PagedList<Record>>()
        val observer: Observer<PagedList<Record>> = mockk()
        every { observer.onChanged(capture(slot)) } just runs

        repo.searchRecordsInRange(OffsetDateTime.MIN, OffsetDateTime.MAX)
                .toLiveData(1)
                .observeForever(observer)

        Assertions.assertThat(slot.captured).contains(*listOf(record).toTypedArray())
        verify(exactly = 1) { observer.onChanged(any()) }
        verify(exactly = 1) { db.getDrinkRecordDao().searchRecordsInRange(any(), any()) }
    }

    @Test
    fun `search records in date range and drink type`() {
        every { db.getDrinkRecordDao().searchRecordsInRangeAndType(any(), any(), any()) } returns DataSourceFactoryMock(listOf(recordDb))
        val slot = slot<PagedList<Record>>()
        val observer: Observer<PagedList<Record>> = mockk()
        every { observer.onChanged(capture(slot)) } just runs

        repo.searchRecordsInRange(OffsetDateTime.MIN, OffsetDateTime.MAX, setOf(DrinkType.BEER))
                .toLiveData(1)
                .observeForever(observer)

        Assertions.assertThat(slot.captured).contains(*listOf(record).toTypedArray())
        verify(exactly = 1) { observer.onChanged(any()) }
        verify(exactly = 1) { db.getDrinkRecordDao().searchRecordsInRangeAndType(any(), any(), any()) }
    }
}